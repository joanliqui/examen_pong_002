/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING, CONTROLS } GameScreen;
typedef enum GameDificult { Nada, Easy, NotEasy } GameDificult;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 900;
    int screenHeight = 700;
    char windowTitle[30] = "raylib game - FINAL PONG";
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = ENDING;
    int framesCounter = 0;          // General pourpose frames counter
    int secondsCounter = 99;
    
    //Variables LOGO
    bool fadeOut = true;
    float alpha = 0;
    float alphaSpeed= 0.01f;
    Texture2D texture1;
    Music logoMusic;
    bool lanzador = false;
    
    //Variables TITLE
    Font demonized;
    Music titleMusic;
    Texture2D titleTexture;
    Texture2D soundOn;
    Texture2D soundOff;
    char title1[7]="FINAL";
    char title2[7]="PONG";
    char startText[20]="PRESS ENTER";
    int fontSize=70;
    bool blink = false;
    Vector2 titlePos1 = {-500,100};
    Vector2 titlePos2 = {screenWidth+250,100};
    
    //Botones
    int botonMargin = 3;
    bool botonPass = false;
    bool mouseOnEasy = false;
    bool mouseOnHard = false;
    bool mouseOnControls = false;
    bool soundLanzador = true;
    GameDificult dificultad = Nada;
    //EASY
    Rectangle subeasymode;
    subeasymode.width = 100;
    subeasymode.height = 50;
    subeasymode.x = screenWidth/2 - subeasymode.width/2;
    subeasymode.y = screenHeight/2 - 100;
    
    Rectangle easymode;
    easymode.width = subeasymode.width - botonMargin*2;
    easymode.height = subeasymode.height - botonMargin*2;
    easymode.x = subeasymode.x + botonMargin;
    easymode.y = subeasymode.y + botonMargin;
    char easyText[20]="Easy";
    //NOT-EASY
    Rectangle subhardmode;
    subhardmode.width = subeasymode.width;
    subhardmode.height = subeasymode.height;
    subhardmode.x = subeasymode.x;
    subhardmode.y = screenHeight/2;
    
    Rectangle hardmode;
    hardmode.width = subeasymode.width - botonMargin*2;
    hardmode.height = subeasymode.height - botonMargin*2;
    hardmode.x = subhardmode.x + botonMargin;
    hardmode.y = subhardmode.y + botonMargin;
    
    char hardText1[7]="NOT";
    char hardText2[7]= "EASY";
    char notYetText[30] = "I'm Working on it";
    float notYetSize = 20.0f;
    
    //Controls
    Rectangle subcontrols;
    subcontrols.width = subeasymode.width;
    subcontrols.height = subeasymode.height;
    subcontrols.x = subeasymode.x;
    subcontrols.y = screenHeight/2 + 100;
    
    Rectangle controls;
    controls.width = subeasymode.width - botonMargin*2;
    controls.height = subeasymode.height - botonMargin*2;
    controls.x = subcontrols.x + botonMargin;
    controls.y = subcontrols.y + botonMargin;
    char controlsText[20]= "CONTROLS";
    float controlsSize= 17;
    
    //Sound On
    Vector2 soundPosition = { screenWidth-100, screenHeight-100};
    
    //Variables CONTROLS
    int controlMargin = 100;
    int controlBgMargin = 5;
    bool timeUp = false;
    Texture2D arriba;
    Texture2D abajo;
    Texture2D espacio;
    Texture2D m;
    Texture2D p;
    
    Rectangle subcontrolRec;
    subcontrolRec.width = screenWidth - controlMargin*2;
    subcontrolRec.height = screenHeight - 100;
    subcontrolRec.x = screenWidth/2 - subcontrolRec.width/2;
    subcontrolRec.y = screenHeight/2 - subcontrolRec.height/2;
    
    Rectangle controlRec;
    controlRec.width = subcontrolRec.width - controlBgMargin*2;
    controlRec.height = subcontrolRec.height - controlBgMargin*2;
    controlRec.x = subcontrolRec.x + controlBgMargin;
    controlRec.y = subcontrolRec.y + controlBgMargin;
    
    char BStoRet[20]= "BackSpace to Return";
    Vector2 arribaPosition = { controlRec.x + 20, controlRec.y + 20};
    Vector2 abajoPosition = { arribaPosition.x, arribaPosition.y + 120};
    Vector2 espacioPosition = { arribaPosition.x, abajoPosition.y + 120};
    Vector2 mPosition = { arribaPosition.x, espacioPosition.y + 120};
    Vector2 pPosition = { arribaPosition.x, mPosition.y + 120};
    
    //Variables GAMEPLAY
    Texture2D gameplayTexture;
    int SonidoPlayer;
    int SonidoEnemy;
    Sound Omar1;
    Sound Omar2;
    Sound Omar3;
    Sound Omar4;
    
    Sound Linan1;
    Sound Linan2;
    Sound Linan3;
    Sound Linan4;
    
    Sound Rebote;
    
    Music musicGameplay;
    
    //Variables que gestiona el player
    Rectangle player;
    player.width= 20;
    player.height=100;
    player.x= 20;
    player.y= screenHeight/2 - player.height/2;
    int playerSpeedY=7;
    //supershot
    int shotMargin = 2;
    int shotCharge = (player.height - shotMargin*2)/10;
    bool specialshot = false;
    bool shotprepared = false;
    
    Rectangle playershot;
    playershot.width = player.width - shotMargin*2;
    playershot.height = 0;
    playershot.x = player.x + shotMargin;
    playershot.y = player.y + shotMargin;
   
    
    //Variables que gestiona el enemy
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 100;
    enemy.x =screenWidth -(enemy.width + 20);
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = 7;
    float AImargin = enemy.height/4;
    
    //Variables que gestiona la ball
    Vector2 ballPosition = {screenWidth/2,screenHeight/2};
    float maxSpeed= 5.0f;
    Vector2 ballSpeed;
    ballSpeed.x=-maxSpeed;
    ballSpeed.y=-maxSpeed;
    if(GetRandomValue(0, 1)) ballSpeed.x *= -1;
    if(GetRandomValue(0, 1)) ballSpeed.y *= -1;
    int ballRadius=20;
    
    //Variables que gestionan el campo
    float topMargin= 65;
    float fondoMargin = 5;
    float countMargin = 35;
    float lifeMargin = 3;
    
    Rectangle fondo = {0, topMargin, screenWidth, screenHeight - topMargin};
    Vector2 middle = {fondo.x + fondo.width/2, fondo.y};
    Vector2 middleEnd = {fondo.x + fondo.width/2, fondo.y + fondo.height};
    
    //Variables que gestiona el Pause
    bool pause = false;
    Font maquina;
    char textPause[20]="GAME PAUSED";
    char returnText[20]= "RETURN";
    Vector2 arribaPosition2 = { screenWidth/2 - 25, screenHeight - 250};
    Vector2 abajoPosition2 = { arribaPosition2.x, screenHeight - 190 };
    Vector2 espacioPosition2 = { arribaPosition2.x - 15, screenHeight - 125 };
    
    Rectangle subpauseReturn;
    subpauseReturn.width = 100;
    subpauseReturn.height = 50;
    subpauseReturn.x = screenWidth/2 - subpauseReturn.width/2;
    subpauseReturn.y = screenHeight/2 - 70;
    
     Rectangle pauseReturn;
    pauseReturn.width = subpauseReturn.width - botonMargin*2;
    pauseReturn.height = subpauseReturn.height - botonMargin*2;
    pauseReturn.x = subpauseReturn.x + botonMargin;
    pauseReturn.y = subpauseReturn.y + botonMargin;
    
    
    //Variables que gestionan la Bar Life
    //Player
    Rectangle plbordeLife = {0, 0, screenWidth/2 - countMargin, topMargin};
    Rectangle plfillLife = {plbordeLife.x + lifeMargin, plbordeLife.y+ lifeMargin, plbordeLife.width - lifeMargin*2, plbordeLife.height-lifeMargin*2};
    Rectangle plLife = plfillLife;
    int playerLife = 5;
    //Enemy
    Rectangle enbordeLife = {screenWidth/2 + countMargin, 0, screenWidth/2 - countMargin, topMargin};
    Rectangle enfillLife = {enbordeLife.x + lifeMargin, enbordeLife.y+ lifeMargin, enbordeLife.width - lifeMargin*2, enbordeLife.height-lifeMargin*2};
    Rectangle enLife = enfillLife;
    int enemyLife = 5;
    
    float playerHurt = plLife.width /playerLife;
    float enemyHurt = enLife.width /enemyLife;
    
    //Variables que gestionan el ENDGAME
    int gameResult = 1;        // 0 - Loose, 1 - Win, 2 - Draw, -1 - Not defined
    int endSize = 50;
    int endSize2 = 80;
    Font defused;
    Font winfont;
    Font drawfont;
    char loseText[15]= "YOU LOSE";
    char winText[15]= "YOU WIN";
    char drawText[15]= "DRAW GAME";
    char endretryText[7]= "RETRY";
    char endreturnText[7]= "RETURN";
    char endexitText[7]= "EXIT";
    Texture2D winTexture;
    Texture2D loseTexture;
    Texture2D drawTexture;
    Music loseMusic;
    Music winMusic;
    Music drawMusic;
    //Boton Retry
    Rectangle subRetry;
    subRetry.width = subeasymode.width;
    subRetry.height = subeasymode.height;
    subRetry.x = screenWidth/2 - subeasymode.width/2;
    subRetry.y = screenHeight/2 - 100;
    
    Rectangle Retry;
    Retry.width = subeasymode.width - botonMargin*2;
    Retry.height = subeasymode.height - botonMargin*2;
    Retry.x = subRetry.x + botonMargin;
    Retry.y = subRetry.y + botonMargin;
    
    //Boton Return
    Rectangle subReturn;
    subReturn.width = subeasymode.width;
    subReturn.height = subeasymode.height;
    subReturn.x = screenWidth/2 - subeasymode.width/2;
    subReturn.y = screenHeight/2;
    
    Rectangle Return;
    Return.width = subeasymode.width - botonMargin*2;
    Return.height = subeasymode.height - botonMargin*2;
    Return.x = subReturn.x + botonMargin;
    Return.y = subReturn.y + botonMargin;
    
    //Boton Exit
    Rectangle subExit;
    subExit.width = subeasymode.width;
    subExit.height = subeasymode.height;
    subExit.x = screenWidth/2 - subeasymode.width/2;
    subExit.y = screenHeight/2 + 100;
    
    Rectangle Exit;
    Exit.width = subeasymode.width - botonMargin*2;
    Exit.height = subeasymode.height - botonMargin*2;
    Exit.x = subExit.x + botonMargin;
    Exit.y = subExit.y + botonMargin;
    
    
    //Declaro Texturas (after InitWindow)
    texture1 = LoadTexture("resources/ps1_logo.png");
    titleTexture = LoadTexture("resources/estrellas.png");
    soundOn = LoadTexture ("resources/sound_on.png");
    soundOff = LoadTexture ("resources/sound_off.png");
    gameplayTexture = LoadTexture ("resources/batalla.png");
    arriba = LoadTexture ("resources/arriba.png");
    abajo = LoadTexture ("resources/abajo.png");
    espacio = LoadTexture ("resources/espacio.png");
    m = LoadTexture ("resources/m.png");
    p = LoadTexture ("resources/p.png");
    winTexture = LoadTexture ("resources/heaven.png");
    loseTexture = LoadTexture ("resources/inferno.png");
    drawTexture = LoadTexture ("resources/gray.png");
    
    //Declaro Fonts (after InitWindow)
    demonized = LoadFont("resources/demonized.ttf");
    maquina = LoadFont("resources/maquina.ttf");
    defused = LoadFont("resources/defused.ttf");
    winfont = LoadFont("resources/funplay.ttf");
    drawfont = LoadFont("resources/blacktail.ttf");
    //Declaro Sounds y Music (after InitAudioDevice)
    InitAudioDevice();
    
    logoMusic = LoadMusicStream("resources/sonidops1.ogg");
    titleMusic = LoadMusicStream("resources/Twilight.ogg");
    loseMusic = LoadMusicStream("resources/wii_song_uncomfortable.ogg");
    winMusic = LoadMusicStream("resources/wii_song_september.ogg");
    drawMusic = LoadMusicStream("resources/best_friend.ogg");
    musicGameplay = LoadMusicStream("resources/zelda.ogg");
    Omar1 = LoadSound("resources/omar_001.ogg");
    Omar2 = LoadSound("resources/omar_002.ogg");
    Omar3 = LoadSound("resources/omar_003.ogg");
    Omar4 = LoadSound("resources/omar_004.ogg");
    Linan1 = LoadSound("resources/Linan_001.ogg");
    Linan2 = LoadSound("resources/Linan_002.ogg");
    Linan3 = LoadSound("resources/Linan_003.ogg");
    Linan4 = LoadSound("resources/Linan_004.ogg");
    Rebote = LoadSound("resources/rebote.wav");
    //VECTOR2
        Vector2 textPausePosition = {screenWidth/2 - MeasureTextEx( maquina, textPause, 50, 0).x/2, screenHeight/2 - 200};
        Vector2 losePosition = { screenWidth/2 - MeasureTextEx(defused, loseText, endSize, 0).x/2, 100};
        Vector2 winPosition = { screenWidth/2 - MeasureTextEx(winfont, winText, endSize2, 0).x/2, 100};
        Vector2 drawPosition = {screenWidth/2 - MeasureTextEx(drawfont, drawText, endSize2, 0).x/2, 100};
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                framesCounter++;
                
                if(!lanzador){
                    PlayMusicStream(logoMusic);
                    lanzador=!lanzador;
                }
                UpdateMusicStream(logoMusic);
                if (fadeOut){
                    alpha+=alphaSpeed;
                    if(alpha>=1.0f){
                        alpha=1.0f;
                        fadeOut=!fadeOut;
                    }
                }
                else {
                    alpha-=alphaSpeed;
                    if(alpha<=0){
                        alpha=0;
                    }
                }
                if(framesCounter==300){
                    framesCounter=0;
                    fadeOut = true;
                    screen=TITLE;
                }
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                //Musica de Fondo
                if(soundLanzador){
                PlayMusicStream(titleMusic);
                UpdateMusicStream(titleMusic);
                }
                if(IsKeyPressed(KEY_M)) soundLanzador= !soundLanzador;
                //Aparicion Fondo
                if(framesCounter<=300){
                    framesCounter++;
                    alpha += alphaSpeed;
                }
                else{
                    alpha = 1;
                }
                
                //Movimiento del "FINAL"
                if(titlePos1.x<(screenWidth/2-MeasureTextEx( demonized, title1, fontSize, 0).x)){
                    titlePos1.x+=5.7;
                }
                else{
                    titlePos1.x = screenWidth/2-MeasureTextEx( demonized, title1, fontSize, 0).x;
                }
                
                //Movimiento del "PONG"
                if(titlePos2.x > (screenWidth/2+10)){
                    titlePos2.x-=5.2;
                }
                else{
                    titlePos2.x = screenWidth/2+10;
                    framesCounter++;
                    botonPass = true;
                    if(CheckCollisionPointRec(GetMousePosition(), easymode)){
                        mouseOnEasy = true;
                        if(mouseOnEasy){
                            if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                                dificultad = Easy;
                            }
                            
                        }
                    }
                    else mouseOnEasy = false;
                    
                    if(CheckCollisionPointRec(GetMousePosition(), hardmode)){
                        mouseOnHard = true;
                        if(mouseOnHard){
                            if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                                blink = false;
                                dificultad = NotEasy;
                            }
                            
                        }
                    }
                    else mouseOnHard = false;
                    
                    if(dificultad == Easy){
                        if (framesCounter%15 == 0){
                            blink = !blink;
                        }
                        if (IsKeyPressed(KEY_ENTER)){
                            framesCounter=0;
                            StopMusicStream(titleMusic);
                            soundLanzador = true;
                            screen=GAMEPLAY;
                        }
                    }
                    
                    if(CheckCollisionPointRec( GetMousePosition(), controls)){
                        mouseOnControls = true;
                        if(mouseOnControls){
                            if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                                framesCounter = 0;
                                screen = CONTROLS;
                            }
                        }
                    }
                    else mouseOnControls = false; 
                }
                
            } break;
            case CONTROLS:
            {
                framesCounter ++;
                dificultad = Nada;
                blink = false;
                if(soundLanzador){
                    PlayMusicStream(titleMusic);
                    UpdateMusicStream(titleMusic);
                }
                if(IsKeyPressed(KEY_M)) soundLanzador= !soundLanzador;
                if(IsKeyPressed(KEY_BACKSPACE)) screen=TITLE;
                if (framesCounter%60 == 0){
                    timeUp = true;
                }
                
            } break;
            case GAMEPLAY:
            { 
                SonidoPlayer = GetRandomValue( 1, 4);
                SonidoEnemy = GetRandomValue( 1, 4);
                //Musica
                if(soundLanzador){
                PlayMusicStream(musicGameplay);
                UpdateMusicStream(musicGameplay);
                }
                if(IsKeyPressed(KEY_M)) soundLanzador= !soundLanzador;
                // Update GAMEPLAY screen data here!
                if (!pause){
                    //mirar si se ha pulsado arriba o abajo
                    if (IsKeyDown(KEY_DOWN)){
                        player.y+=playerSpeedY;
                        playershot.y+=playerSpeedY;
                    }
                    if (IsKeyDown(KEY_UP)){
                        player.y-=playerSpeedY;
                        playershot.y-=playerSpeedY;
                    }
                    //Mirar limites del player y el shot
                    if (player.y < topMargin + fondoMargin){
                        player.y = topMargin + fondoMargin;
                        playershot.y = topMargin + fondoMargin + shotMargin;
                    }
                    if (player.y>screenHeight-player.height-fondoMargin){
                        player.y = screenHeight-player.height-fondoMargin;
                        playershot.y = screenHeight - player.height - shotMargin;
                    }
                    //Mirar la nueva posicion de la pelota
                    ballPosition.x+=ballSpeed.x;
                    ballPosition.y+=ballSpeed.y;
                    //Mirar limites en X negativa de la ball
                    if((ballPosition.x-ballRadius) <= fondoMargin && ballSpeed.x < 0){
                        ballSpeed.x=maxSpeed;
                        PlaySound(Rebote);
                        //Logica Player Bar Life
                        playerLife--;
                        plLife.width -= playerHurt;     
                    }
                    //Mirar limites en X positiva
                    if((ballPosition.x+ballRadius)> screenWidth-fondoMargin && ballSpeed.x > 0 )
                    {
                        ballSpeed.x=-maxSpeed;
                        PlaySound(Rebote);
                        //Logica Enemy Bar Life
                        enemyLife--;
                        enLife.width -= enemyHurt;
                        enLife.x += enemyHurt;
                    }
                    //Mirar limites en Y negativa
                    if((ballPosition.y-ballRadius)<topMargin+fondoMargin){
                        ballSpeed.y *= -1;
                        PlaySound(Rebote);
                    }
                    //Mirar limites en Y positiva
                    if((ballPosition.y+ballRadius)>screenHeight-fondoMargin){
                        ballSpeed.y *= -1;
                        PlaySound(Rebote);
                    }
                    //Mirar choque con player
                        if(CheckCollisionCircleRec(ballPosition, ballRadius, player)){
                            //Sound
                            switch(SonidoPlayer)
                            {
                                case 1:
                                {
                                    PlaySound(Omar1);  
                                } break;
                                case 2:
                                {
                                    SetSoundVolume ( Omar2, 0.2);
                                    PlaySound(Omar2);
                                } break;
                                case 3:
                                {
                                    SetSoundVolume ( Omar3, 0.5);
                                    PlaySound(Omar3);
                                } break;
                                case 4:
                                {
                                    SetSoundVolume ( Omar4, 0.5);
                                    PlaySound(Omar4);
                                }
                            }
                            
                            
                            ballPosition.x = player.x + ballRadius + player.width;
                            ballSpeed.x *= -1.2;
                            playershot.height += shotCharge;
                            if(playershot.height >= player.height - shotMargin*2-1){
                                playershot.height = player.height - shotMargin*2-1;
                                specialshot = true;
                            }
                            
                        }   
                    //Special Shot
                    if(specialshot){
                        if(IsKeyPressed(KEY_SPACE)){
                            shotprepared = true;
                            }
                        }
                        if(shotprepared){
                            if(CheckCollisionCircleRec( ballPosition, ballRadius, player)){
                                ballPosition.x = player.x + ballRadius + player.width;
                                ballSpeed.x = 100; //PROBLEM ALERT!!!!
                                playershot.height = 0;
                                specialshot = false;
                                shotprepared = false;
                            }
                        }

                    //Mirar choque con enemy
                    if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy)){
                        //Sound
                        switch(SonidoEnemy)
                        {
                            case 1:
                            {
                                SetSoundVolume ( Linan1, 0.4);
                                PlaySound(Linan1);
                            } break;
                            case 2:
                            {
                                SetSoundVolume ( Linan2, 0.4);
                                PlaySound(Linan2);
                            } break;
                            case 3:
                            {
                                SetSoundVolume ( Linan3, 0.4);
                                PlaySound(Linan3);
                            } break;
                            case 4:
                            {
                                SetSoundVolume ( Linan4, 0.4);
                                PlaySound(Linan4);
                            } break;
                            
                        }
                        
                        
                        ballPosition.x= enemy.x - ballRadius - enemy.width;
                        ballSpeed.x*=-1.2;
                    }
                    //Mirar limites del enemy
                    if(enemy.y < topMargin + fondoMargin){
                        enemy.y = topMargin + fondoMargin;
                    }
                    if(enemy.y > screenHeight - player.height - fondoMargin){
                        enemy.y = screenHeight - enemy.height - fondoMargin;
                    }
                    //Logica Contador
                    framesCounter++;
                    if (framesCounter>=60){
                        secondsCounter--;
                        framesCounter=0;
                        if (secondsCounter==0){
                            screen=ENDING;
                        }
                    }
                    //Movimiento del enemy
                    if((ballPosition.x > screenWidth/2) && ballSpeed.x > 0){
                        if(ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                        if(ballPosition.y > enemy.y + enemy.height/2 + AImargin) enemy.y += enemySpeedY;
                    }
                    //Logiga del END GAME
                    if (secondsCounter <=0){
                        if(playerLife < enemyLife) gameResult = 0;
                        else if(playerLife > enemyLife) gameResult = 1;
                        else gameResult = 2;
                    }
                    else if(playerLife <= 0) gameResult = 0;
                    else if(enemyLife <= 0) gameResult = 1;
                    
                    if(gameResult != -1){
                        StopMusicStream(musicGameplay);
                        screen = ENDING;
                    }
                }
                if(IsKeyPressed('p') || IsKeyPressed('P')){
                    pause=!pause;
                }
                if(pause){
                    //BOTON RETURN
                    if(CheckCollisionPointRec(GetMousePosition(), pauseReturn)){
                        mouseOnEasy = true;
                        if(mouseOnEasy){
                            if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                        framesCounter = 0;
                        secondsCounter = 99;
                        dificultad = Nada;
                        player.y = screenHeight/2 - player.height/2;
                        specialshot = false;
                        shotprepared = false;
                        playershot.y = player.y + shotMargin;
                        playershot.height = 0;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        ballPosition = (Vector2){screenWidth/2,screenHeight/2};
                        ballSpeed.x = -maxSpeed;
                        ballSpeed.y= -maxSpeed;
                        pause = false;
                        plLife = plfillLife;
                        playerLife = 5;
                        enLife = enfillLife;
                        enemyLife = 5;
                        gameResult = -1;
                        mouseOnEasy = false;
                        mouseOnHard = false;
                        mouseOnControls = false;
                        soundLanzador = true;
                        timeUp = false;
                        blink = false;
                        
                        screen = TITLE;
                            }
                        }
                    }
                    else  mouseOnEasy = false;
                    
                }
                
            } break;
            case ENDING: 
            {
                //Musica
                if(gameResult == 0){
                    PlayMusicStream(loseMusic);
                    UpdateMusicStream(loseMusic);
                }
                else if(gameResult == 1){
                    PlayMusicStream(winMusic);
                    UpdateMusicStream(winMusic);
                }
                else{
                    PlayMusicStream(drawMusic);
                    UpdateMusicStream(drawMusic);
                }
                //Retry
                if(CheckCollisionPointRec( GetMousePosition(), Retry)){
                    mouseOnEasy = true;
                    if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                        framesCounter = 0;
                        secondsCounter = 99;
                        dificultad = Easy;
                        player.y = screenHeight/2 - player.height/2;
                        specialshot = false;
                        shotprepared = false;
                        playershot.y = player.y + shotMargin;
                        playershot.height = 0;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        ballPosition = (Vector2){screenWidth/2,screenHeight/2};
                        ballSpeed.x = -maxSpeed;
                        ballSpeed.y= -maxSpeed;
                        pause = false;
                        plLife = plfillLife;
                        playerLife = 5;
                        enLife = enfillLife;
                        enemyLife = 5;
                        gameResult = -1;
                        StopMusicStream(loseMusic);
                        StopMusicStream(winMusic);
                        screen = GAMEPLAY;
                    }
                }
                else mouseOnEasy = false;
                
                //Return
                if(CheckCollisionPointRec( GetMousePosition(), Return)){
                    mouseOnHard = true;
                    if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                        framesCounter = 0;
                        secondsCounter = 99;
                        dificultad = Nada;
                        player.y = screenHeight/2 - player.height/2;
                        specialshot = false;
                        shotprepared = false;
                        playershot.y = player.y + shotMargin;
                        playershot.height = 0;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        ballPosition = (Vector2){screenWidth/2,screenHeight/2};
                        ballSpeed.x = -maxSpeed;
                        ballSpeed.y= -maxSpeed;
                        pause = false;
                        plLife = plfillLife;
                        playerLife = 5;
                        enLife = enfillLife;
                        enemyLife = 5;
                        gameResult = -1;
                        mouseOnEasy = false;
                        mouseOnHard = false;
                        mouseOnControls = false;
                        soundLanzador = true;
                        timeUp = false;
                        blink = false;
                        StopMusicStream(loseMusic);
                        StopMusicStream(winMusic);
                        
                        screen = TITLE;
                    }
                }
                else mouseOnHard = false;
                
                //Exit
                if(CheckCollisionPointRec( GetMousePosition(), Exit)){
                    mouseOnControls = true;
                    if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                        return 0;
                    }
                }
                else mouseOnControls = false;
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    DrawTexture(texture1,screenWidth/2-texture1.width/2,screenHeight/2-texture1.height/2,Fade(WHITE, alpha));
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawTexture(titleTexture, 0, 0, Fade(WHITE, alpha));
                    DrawTextEx(demonized, title1, titlePos1, fontSize, 0, BLACK);
                    DrawTextEx(demonized, title2, titlePos2, fontSize, 0, BLACK);
                    //BOTONES
                    //Easy
                    if( botonPass){
                    DrawRectangleRec( subeasymode, BLACK);
                    DrawRectangleGradientH( easymode.x, easymode.y, easymode.width, easymode.height, BLUE, VIOLET);
                    if(mouseOnEasy || dificultad==Easy) DrawRectangleRec(easymode, DARKBLUE);
                    DrawText( easyText,easymode.x + easymode.width/2- MeasureText( easyText, 30)/2, easymode.y + botonMargin, 30, BLACK);
                    }
                    //Hard
                    if(botonPass){
                        DrawRectangleRec( subhardmode, BLACK);
                        DrawRectangleGradientH( hardmode.x, hardmode.y, hardmode.width, hardmode.height, VIOLET, BLUE);
                        if(mouseOnHard || dificultad==NotEasy) DrawRectangleRec(hardmode, VIOLET);
                        DrawText( hardText1, hardmode.x + hardmode.width/2 - MeasureText( hardText1, 20)/2, hardmode.y + botonMargin, 20, BLACK);
                        DrawText( hardText2, hardmode.x + hardmode.width/2 - MeasureText( hardText2, 20)/2, hardmode.y + botonMargin + 20, 20, BLACK);
                    }
                    if(dificultad == NotEasy){
                        DrawText(notYetText, screenWidth/2 - MeasureText(notYetText,notYetSize)/2, screenHeight-100, notYetSize,RED);
                    } 
                   
                    if (blink){
                        DrawText(startText, screenWidth/2 - MeasureText(startText,30)/2, screenHeight-100, 30, WHITE);
                    }
                    //Controls
                    if(botonPass){
                        DrawRectangleRec( subcontrols,BLACK);
                        DrawRectangleGradientH(controls.x, controls.y, controls.width, controls.height, RED, ORANGE);
                        if(mouseOnControls) DrawRectangleRec( controls, RED);
                        DrawText(controlsText, screenWidth/2 - MeasureText(controlsText, controlsSize)/2, screenHeight/2 + botonMargin + 110, controlsSize, BLACK);
                        //Sound On-Off
                        if(soundLanzador){
                            DrawTextureEx(soundOn, soundPosition, 0, 0.2f, WHITE);
                        }
                        else DrawTextureEx(soundOff, soundPosition, 0, 0.13f, WHITE);
                    }
                    
                } break;
                case CONTROLS:
                {
                    DrawTexture(titleTexture, 0, 0, Fade(WHITE, alpha));
                    DrawRectangleRec( subcontrolRec, BLACK);
                    DrawRectangleGradientV(controlRec.x, controlRec.y, controlRec.width, controlRec.height, BLUE, SKYBLUE);
                    DrawTextureEx( arriba, arribaPosition, 0, 0.2, WHITE);
                    DrawTextureEx( abajo, abajoPosition, 0, 0.2, WHITE);
                    DrawTextureEx( espacio, espacioPosition, 0, 0.3, WHITE);
                    DrawTextureEx( m, mPosition, 0, 0.3, WHITE);
                    DrawTextureEx( p, pPosition, 0, 0.3, WHITE);
                    DrawText("UP", arribaPosition.x + 100, arribaPosition.y + 30, 30, BLACK);
                    DrawText("DOWN", abajoPosition.x + 100, abajoPosition.y + 30, 30, BLACK);
                    DrawText("SUPER SHOT", espacioPosition.x + 150, espacioPosition.y + 20, 30, BLACK);
                    DrawText("MUTE", mPosition.x + 80, mPosition.y + 10, 30, BLACK);
                    DrawText("PAUSE", pPosition.x + 80, pPosition.y + 10, 30, BLACK);
                    DrawText( BStoRet, screenWidth/2 + 90, 20, 20, ORANGE);
                    if(timeUp){
                        
                    }
                    if(soundLanzador){
                            DrawTextureEx(soundOn, soundPosition, 0, 0.2f, WHITE);
                        }
                    else DrawTextureEx(soundOff, soundPosition, 0, 0.13f, WHITE);
                }break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    //Pinto Escenario
                    DrawTexture(gameplayTexture, 0, 0, WHITE);
                    DrawRectangleLinesEx( fondo, fondoMargin, BLACK);
                    DrawLineEx(middle, middleEnd, fondoMargin, BLACK);
                    
                    //Pinto Player
                    DrawRectangleRec(player,BLACK);
                    //playershot
                    DrawRectangleRec( playershot, BLUE);
                    if(specialshot) DrawRectangleGradientV(playershot.x, playershot.y, playershot.width, playershot.height, RED, ORANGE);
                    if(shotprepared) DrawRectangleRec( playershot, RED);
                    //Pinto Enemy
                    DrawRectangleRec(enemy,GRAY);
                    //Pinto pelota
                    DrawCircleV(ballPosition, ballRadius, RED);
                    //Pinto contador
                    DrawText(FormatText("%d", secondsCounter),screenWidth/2 - MeasureText(FormatText("%d", secondsCounter), 50)/2, 14, 50, RED);
                    //Pinto Barra de vida
                    //PLAYER
                    DrawRectangleRec(plbordeLife,BLACK);
                    DrawRectangleGradientH(plfillLife.x, plfillLife.y, plfillLife.width, plfillLife.height, BLACK, GRAY);
                    //DrawRectangleRec(plfillLife, GRAY);
                    DrawRectangleGradientH(plLife.x, plLife.y, plLife.width, plLife.height, RED, GOLD);
                    //ENEMY
                    DrawRectangleRec(enbordeLife,BLACK);
                    DrawRectangleGradientH(enfillLife.x, enfillLife.y, enfillLife.width, enfillLife.height, GOLD, RED);
                    //DrawRectangleRec(enfillLife,RED);
                    DrawRectangleGradientH(enLife.x, enLife.y, enLife.width, enLife.height, GRAY, BLACK);
                    //Pinto Pause
                    if(pause){
                        DrawRectangle(0,0, screenWidth, screenHeight, Fade(WHITE, 0.8f));
                        DrawTextEx(maquina, textPause, textPausePosition, 50, 0, BLACK);
                        DrawRectangleRec( subpauseReturn, BLACK);
                        DrawRectangleRec(pauseReturn, GREEN);
                        if(mouseOnEasy) DrawRectangleRec(pauseReturn, DARKGREEN);
                        DrawText(returnText, pauseReturn.x + 5, pauseReturn.y + 10,20,BLACK);
                        DrawText(controlsText, screenWidth/2 - MeasureText(controlsText, 20)/2, pauseReturn.y + pauseReturn.width + 40, 20, BLACK);
                        DrawTextureEx( arriba, arribaPosition2, 0, 0.15, WHITE);
                        DrawTextureEx( abajo, abajoPosition2, 0, 0.15, WHITE);
                        DrawTextureEx( espacio, espacioPosition2, 0, 0.2, WHITE);
                        
                        
                    }
                    
                    
                                        
                } break;
                case ENDING: 
                {
                    //Pinto botones
                    if(gameResult!= -1){
                        //Pinto derrota
                        if(gameResult==0){
                            DrawTexture(loseTexture, 0, 0, WHITE);
                            DrawTextEx(defused, loseText, losePosition, endSize, 0, RED);
                        }
                        //Pinto victoria
                        else if(gameResult == 1){
                            DrawTexture(winTexture, 0, 0, WHITE);
                            DrawTextEx(winfont, winText, winPosition, endSize2, 0, GREEN);
                        } 
                        //Pinto empate
                        else if(gameResult == 2){
                            DrawTexture(drawTexture, 0, 0, WHITE);
                            DrawTextEx(drawfont, drawText, drawPosition, endSize2, 0, BLACK);
                        } 
                        DrawRectangleRec(subRetry, BLACK);
                        DrawRectangleGradientH(Retry.x, Retry.y, Retry.width, Retry.height, DARKGREEN, GREEN);
                        if(mouseOnEasy) DrawRectangleRec( Retry, DARKGREEN);
                        DrawText(endretryText, Retry.x + 5, Retry.y + 10, 25, BLACK);
                        DrawRectangleRec(subReturn, BLACK);
                        DrawRectangleGradientH(Return.x, Return.y, Return.width, Return.height, VIOLET, PURPLE);
                        if(mouseOnHard) DrawRectangleRec( Return, VIOLET);
                        DrawText(endreturnText,Return.x + 5, Return.y + 10, 20, BLACK);
                        DrawRectangleRec(subExit, BLACK);
                        DrawRectangleGradientH(Exit.x, Exit.y, Exit.width, Exit.height, RED, ORANGE);
                        if(mouseOnControls) DrawRectangleRec( Exit, RED);
                        DrawText( endexitText, Exit.x + 10, Exit.y + 10, 30, BLACK);
                    }
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadMusicStream(logoMusic);
    UnloadMusicStream(titleMusic);
    UnloadMusicStream(loseMusic);
    UnloadMusicStream(winMusic);
    UnloadMusicStream(drawMusic);
    UnloadMusicStream(musicGameplay);
    UnloadSound(Omar1);
    UnloadSound(Omar2);
    UnloadSound(Omar3);
    UnloadSound(Omar4);
    UnloadSound(Linan1);
    UnloadSound(Linan2);
    UnloadSound(Linan3);
    UnloadSound(Linan4);
    UnloadSound(Rebote);
    UnloadTexture(texture1);
    UnloadTexture(titleTexture);
    UnloadTexture(soundOn);
    UnloadTexture(soundOff);
    UnloadTexture(gameplayTexture);
    UnloadTexture(arriba);
    UnloadTexture(abajo);
    UnloadTexture(espacio);
    UnloadTexture(m);
    UnloadTexture(p);
    UnloadTexture(winTexture);
    UnloadTexture(loseTexture);
    UnloadTexture(drawTexture);
    UnloadFont(demonized);
    UnloadFont(maquina);
    UnloadFont(defused);
    UnloadFont(winfont);
    UnloadFont(drawfont);
    
    CloseAudioDevice();
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}